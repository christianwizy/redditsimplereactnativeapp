export default class RedditClient {
  // this is so you don't have to read Reddits API documentation
  constructor(token) {
    this.baseUrl = 'https://oauth.reddit.com/'
    this.defaultHeaders = {
        'Authorization': `bearer ${token}`,
    }
  }

  // use "fetch" to retrieve data from endpoints on the above baseUrl
  // and add the defaultHeaders to your request
  getPosts = (endpoint) => this.fetchWithHeaders(this.baseUrl + endpoint)
  getRandom = () => this.fetchWithHeaders(this.baseUrl + '/random')

  fetchWithHeaders = (url) => (
    fetch(url, {
      headers: this.defaultHeaders
    }).then((response) => response.json())
  )

}
