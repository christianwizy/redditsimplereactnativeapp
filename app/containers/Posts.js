import React, { Component, PropTypes } from 'react'
import { View, Text, ScrollView, StyleSheet, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import {
  fetchPosts,
  fetchPostsSuccess,
  fetchPostsFailure,
} from '../redux/actions'

import RedditClient from '../api/RedditClient'
import Post from '../components/Post'

const mapStateToProps = (state) => ({
  //fetch relevent redux state and pass into props here
  token: state.user.token,
  posts: state.posts.subreddits.hot,
  postsError: state.posts.error,
  postsTimestamp: state.posts.timestamp,
  loading: state.posts.isFetching
})

class Posts extends Component {
  static propTypes = {
  }

  componentDidMount() {
    //fetch the posts here
    const { token, dispatch, subreddit } = this.props
    if (!token) {
      return Actions.login()
    }
    dispatch(fetchPosts(subreddit))
    this.dispatcher(token, dispatch, subreddit);
  }

  dispatcher = (token, dispatch, subreddit) => {
    new RedditClient(token).getPosts(subreddit)
      .then((result) => {
        if (result.error) {
          dispatch(fetchPostsFailure(
            `${result.error}: ${result.message}`
          ))
        } else {
          let items = null
          if (Array.isArray(result)) {
            items = result.reduce((all, subitems) => all.concat(subitems.data.children), [])
          } else {
            items = result.data.children
          }
          dispatch(fetchPostsSuccess(items, subreddit))
        }
      })
      .catch((error) => {
        dispatch(fetchPostsFailure(error, subreddit))
      })
  }

  componentWillReceiveProps(nextProps) {
    // If we are now logged in, check if we need to fetch posts
    if(!this.props.token !== nextProps.token) {
      this.fetchPostsIfNeeded(nextProps);
    }
  }
  fetchPostsIfNeeded = (nextProps) => {
    const {
      token,
      loading,
      postsTimestamp,
      posts,
      dispatch,
      subreddit,
    } = nextProps || this.props

    if (!token || loading) {
      return
    }

    if (!postsTimestamp || (posts.length === 0 && Date.now() - postsTimestamp > 60 * 1000)) {
      dispatch(fetchPosts())
      this.dispatcher(token, dispatch, subreddit);
    }
  }
  renderPosts = () => {

    // can use this to render posts in the ScrollView
    return (<View>{this.props.posts.map((post, index) => {
      const { title, subreddit, preview } = post.data
      return (
        <Post
          key={index}
          title={title}
          subreddit={subreddit}
          preview={preview}
        />
      )
    })}</View>
    );
  }

  /**
   * Using a ScrollView
   * https://facebook.github.io/react-native/docs/using-a-scrollview.html
   */
  render() {
    return (
      <View>
        <ScrollView>
          {this.props.loading?
            <View style={{flex: 1, backgroundColor: '#eee', padding: 15}}>
                <ActivityIndicator animating={true} />
            </View>
            : this.renderPosts()
          }
        </ScrollView>
      </View>
    )
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1
  },
  error: {
    flex: 1,
    backgroundColor: 'red',
    padding: 15
  },
  loading: {
    flex: 1,
    backgroundColor: '#eee',
    padding: 15
  }
})

export default connect(mapStateToProps)(Posts)
