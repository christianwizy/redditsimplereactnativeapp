import {
  FETCH_POSTS_PENDING,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
} from './constants';

import RedditClient from '../api/RedditClient'
export const fetchPosts = (subreddit) => {
  return {
    type: FETCH_POSTS_PENDING,
    timestamp: Date.now()
  }
}
export const fetchPostsSuccess = (items, subreddit) => {
  return {type: FETCH_POSTS_SUCCESS, subreddit, items}
}
export const fetchPostsFailure = (error) => {
  return {
    type: FETCH_POSTS_FAILURE,
    error
  }
}