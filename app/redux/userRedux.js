import { setToken, getToken, clearToken, tokenHasExpired } from '../api/Storage'

/**
 * This works because of our redux-thunk middleware in ./store/configureStore
 *
 * ...action creators that return a function instead of an action.
 * The thunk can be used to delay the dispatch of an action,
 * or to dispatch only if a certain condition is met.
 * The inner function receives the functions dispatch and getState as parameters.
 */
import {
  START_AUTH,
  AUTH_SUCCESS,
  AUTH_FAIL,
} from './constants'

const startAuthentication = () => async (dispatch) => {
  // Try and retrieve token from Storage

  const tokenExpired = await tokenHasExpired()
  const token = await getToken()
  if (tokenExpired) {
    clearToken()
  }
  return (token && !tokenExpired) ? (
    // succesfully retrieved it
    dispatch(authSuccess(token))
  ) : dispatch(pendingAuth())
}


const pendingAuth = () => {
  return {
    type: START_AUTH,
    payload: {
      isLoading: true,
      error: '',
      token: '',
    }
  };
}

const authSuccess = (token) => {
  setToken(token)
  return {
    type: AUTH_SUCCESS,
    payload: {
      isLoading: false,
      token,
      error: '',
    }
  };
}
const authFail = (error) => {
  return {
    type: AUTH_FAIL,
    payload: {
      isLoading: false,
      token: '',
      error
    }
  };
}
export const actionCreators = {
  startAuthentication,
  pendingAuth,
  authSuccess,
  authFail,
  // add the other action creators
}

const initialState = {
  //setup initialState
  isLoading: false,
  error: '',
  token: '',


}

export const reducer = (state = initialState, action) => {
  const {type, payload} = action
  switch(type) {
    // update state here
    case START_AUTH:
      return {
        ...state,
        ...payload,
      }
    case AUTH_SUCCESS:
      return {
        ...state,
        ...payload,
      }
    case AUTH_FAIL:
      return {
        ...state,
        ...payload,
      }
    default: {
      return state
    }
  }
}
