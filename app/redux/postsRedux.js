import {
  FETCH_POSTS_PENDING,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
} from './constants';

const initialState = {
  isFetching: false,
  token: null,
  timestamp: null,
  subreddits: {
    hot: [],
    random: [],
  }
}

//implement your reducer
export const reducer = (state = initialState, action) => {
  const {type, payload} = action

  switch(type) {
    case FETCH_POSTS_PENDING: {
      return {
        ...state,
        isFetching: true,
        timestamp: action.timestamp,
        error: null
      }
    }
    case FETCH_POSTS_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        subreddits: {
          ...state.subreddits,
          [action.subreddit]: action.items || [],
        },
        error: null
      }
    }
    case FETCH_POSTS_FAILURE: {
      return {
        ...state,
        isFetching: false,
        subreddits: {
          ...state.subreddits,
          [action.subreddit]: [],
        },
        error: action.error
      }
    }
    default: {
      return state
    }
  }
}